from django.views.generic import ListView
from django_htmx.views import HTMXViewMixin

from app import models


class PersonListView(HTMXViewMixin, ListView):
    model = models.Person
    paginate_by = 10
    template_name = 'app/person_list.html'
    partial_name = 'app/partials/person_list.html'
