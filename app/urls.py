from django.urls import path

from app import views

urlpatterns = [
    path('people/list/', views.PersonListView.as_view(), name='people-list'),
]
