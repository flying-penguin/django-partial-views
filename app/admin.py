from django.contrib import admin
from app import models


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    pass
