# Django Partial render exploration
an exploration of rendering html from django over ajax

## Get Started
install poetry then
```shell
poetry install
poetry run python manage.py migrate
poetry run python manage.py createsuperuser
poetry run python manage.py generatedata # will create fake data
poetry run python manage.py runserver
```

head to `/app/people/`

## PAGINATION - How it works
Provides two ListViews, the first is generic and represents the whole page, the second inherits from the first
but renders only a part of the template, which is shared between the two views using {% include directive %}.

Runtime changes are handled via AlpineJS (https://github.com/alpinejs/alpine) which is a small JS utility
to provide reactive behaviour with a declarative syntax.
